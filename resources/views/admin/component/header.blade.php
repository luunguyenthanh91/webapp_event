<div class="app-header header py-1 d-flex">
  <div class="container">
    <div class="d-flex">
      <a class="header-brand" href="/admin">
        <img src="{{ asset('admins/assets/images/brand/logo.png') }}" class="header-brand-img d-none d-sm-block" alt="Spain logo">
        <img src="{{ asset('admins/assets/images/brand/logo.png') }}" class="header-brand-img-2 d-sm-none" alt="Spain logo">
      </a>
      <a id="horizontal-navtoggle" class="animated-arrow"><span></span></a>


        <div class="d-flex order-lg-2 ml-auto">


        <div class="dropdown d-none d-md-flex " >
          <a  class="nav-link icon full-screen-link">
            <i class="mdi mdi-arrow-expand-all"  id="fullscreen-button"></i>
          </a>
        </div>

        <div class="dropdown d-none d-md-flex">
          <a class="nav-link icon" data-toggle="dropdown">
            <i class="fe fe-grid floating"></i>
          </a>
          <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow p-0">
            <ul class="drop-icon-wrap p-0 m-0">
              <li>
                <a href="#" class="drop-icon-item">
                  <i class="fe fe-mail"></i>
                  <span class="block"> E-mail</span>
                </a>
              </li>
              <li>
                <a href="#" class="drop-icon-item">
                  <i class="fe fe-calendar"></i>
                  <span class="block">calendar</span>
                </a>
              </li>
              <li>
                <a href="#" class="drop-icon-item">
                  <i class="fe fe-map-pin"></i>
                  <span class="block">map</span>
                </a>
              </li>
              <li>
                <a href="#" class="drop-icon-item">
                  <i class="fe fe-shopping-cart"></i>
                  <span class="block">Cart</span>
                </a>
              </li>
              <li>
                <a href="#" class="drop-icon-item">
                  <i class="fe fe-message-square"></i>
                  <span class="block">chat</span>
                </a>
              </li>
              <li>
                <a href="#" class="drop-icon-item">
                  <i class="fe fe-phone-outgoing"></i>
                  <span class="block">contact</span>
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div class="dropdown">
          <a href="#" class="nav-link pr-0 leading-none" data-toggle="dropdown">
            <span class="avatar avatar-md brround"><img src="https://static05.cminds.com/wp-content/uploads/computer-1331579_1280-1-300x300.png" alt="Profile-img" class="avatar avatar-md brround"></span>
          </a>
          <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow ">

            <a class="dropdown-item" href="/admin/logout">
              <i class="dropdown-icon mdi  mdi-logout-variant"></i> Sign out
            </a>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!--Horizontal-menu-->
<div class="horizontal-main clearfix">
  <div class="horizontal-mainwrapper container clearfix">
    <nav class="horizontalMenu clearfix">
      <ul class="horizontalMenu-list">
        <li>
          <a href="/admin" class="sub-icon @if( @$menu == 'home') active @endif">
            <i class="fa fa-home"></i> Trang Chủ
          </a>
        </li>
        <li>
          <a href="/admin/users" class="sub-icon @if( @$menu == 'users') active @endif">
            <i class="si si-user"></i> Thành Viên
          </a>
        </li>
        <li>
          <a href="/admin/events" class="sub-icon @if( @$menu == 'events') active @endif">
            <i class="si si-game-controller"></i> Sự Kiện
          </a>
        </li>
        <li>
          <a href="/admin/gift" class="sub-icon @if( @$menu == 'gifts') active @endif">
            <i class="si si-present"></i> Giải Thưởng
          </a>
        </li>
        <li>
          <a href="/admin/history" class="sub-icon @if( @$menu == 'historys') active @endif">
            <i class="si si-notebook"></i> Lịch Sử Quay Thưởng
          </a>
        </li>
        <li>
          <a href="/admin/history-win" class="sub-icon @if( @$menu == 'historys_gift') active @endif">
            <i class="si si-trophy"></i> Danh Sách Trúng Thưởng
          </a>
        </li>


      </ul>
    </nav>
    <!--Menu HTML Code-->
  </div>
</div>
