@extends('layouts.admin_login')

@section('content')
    <div class="page-single">
      <div class="container">
        <div class="row">
          <div class="col mx-auto">
            <div class="text-center mb-6">
              <img src="assets/images/brand/logo.png" class="" alt="">
            </div>
            <div class="row justify-content-center">
              <div class="col-md-8">
                <div class="card-group mb-0">


                  <div class="card p-4">
                    <div class="card-body">
                      <h1>Login</h1>
                      @if( !$errors->isEmpty())
                          <div class="alert alert-danger col-md-12">

                            @foreach($errors->all() as $item)
                                <h4 class="col-md-12 col-12">- {{ $item}}</h4>
                            @endforeach
                          </div>
                      @endif
                      @if(@$message)
                          <div class="alert alert-danger col-md-12">
                              <h4 class="col-md-12 col-12">- {{$message}}</h4>
                          </div>
                      @endif

                      <p class="text-muted">Sign In to your account</p>
                      <form method="post" action="{{ route('login_admin') }}">
                          @csrf
                          <div class="input-group mb-3">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input type="email" required name="email" class="form-control" placeholder="Email">
                          </div>
                          <div class="input-group mb-4">
                            <span class="input-group-addon"><i class="fa fa-unlock-alt"></i></span>
                            <input type="password" required name="password" class="form-control" placeholder="Password">
                          </div>
                          <div class="row">
                            <div class="col-12">
                              <button type="submit" class="btn btn-gradient-primary btn-block">Login</button>
                            </div>
                          </div>
                      </form>


                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection

@section('page-js')

@endsection
