@extends('layouts.admin_main')

@section('content')
<div class="content-area">
  <div class="container">

    <div class="page-header">
      <h4 class="page-title">Sự Kiện</h4>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/admin">Trang Chủ</a></li>
        <li class="breadcrumb-item active" aria-current="page">Quản Lý Sự Kiện</li>
      </ol>
    </div>
    <div class="row row-cards">
      <div class="col-lg-12">
        <div>

          <div class="row row-cards">
							<div class="col-lg-3">
								<div class="row">
									<div class="col-md-12 col-lg-12">
										<div class="card">
											<div class="card-header">
												<div class="card-title"> Điều Kiện Lọc</div>
											</div>
											<div class="card-body">
												<div class="form-group">
													<label class="form-label">Tình Trạng</label>
													<select id="status" name="condition[status]" class="form-control custom-select">
														<option value="">--Tất Cả--</option>
														<option value="1" @if(@$condition['status'] == 1 ) selected @endif>Đang diễn ra</option>
														<option value="2" @if(@$condition['status'] == 2 ) selected @endif>Đã kết thúc</option>
													</select>
                          <button type="button" class="btn btn-primary btn_search col-md-12 col-lg-12  mt-3">
                            <i class="fa fa-search " aria-hidden="true"></i>
                          </button>
												</div>



                        <div class="form-group">
                            <label class="form-label">Thêm Sự Kiện</label>
        										<a href="/admin/add-events" class="btn btn-primary  col-md-12 col-lg-12">
        											<i class="fa fa-calendar-plus-o " aria-hidden="true"></i>
        										</a>
												</div>


                        <form method="post" action="" id="form_search"  >
                            @csrf
                            <input type="hidden" name="condition[status]" id="status_val" />
                            <input type="hidden" name="condition[name]" id="name_val" />
                        </form>


											</div>
										</div>

									</div>
								</div>
							</div>

							<div class="col-lg-9">

								<div class="input-group">
									<input type="text" name="name" id="name" value="{{@$condition['name']}}" class="form-control br-tl-7 br-bl-7" placeholder="Tên sự kiện">
									<div class="input-group-append ">
										<button type="button" class="btn btn_search btn-primary br-tr-7 br-br-7">
											<i class="fa fa-search " aria-hidden="true"></i>
										</button>
									</div>

								</div>

								<div class="card mt-5 store">
                  @if($message != '')
                  <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{$message}}
                  </div>
                  @endif
                  <div class="update_status" style="display:none">
                    <div class="alert alert-success" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        Thay đổi tình trạng thành công.
                    </div>
                  </div>
                  <div class="e-table">
                    <div class="table-responsive table-lg">
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th  class="text-center">
                                #
                            </th>
                            <th class="text-center">Tên sự kiện</th>
                            <th>Mã Code</th>
                            <th>Thời Gian Bắt Đầu</th>
                            <th>Thời Gian Kết Thúc</th>
                            <th>Tình Trạng</th>
                            <th class="text-center"></th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach ($users as $item)
                              <tr>
                                <td class="align-middle text-center">
                                    {{$item->id}}
                                </td>
                                <td class="align-middle text-center">{{$item->name}}</td>
                                <td class="text-center align-middle">{{$item->code}}</td>
                                <td class="text-center align-middle">{{$item->start_date}}</td>
                                <td class="text-center align-middle">{{$item->expired_at}}</td>
                                <td class="align-middle" style="justify-content: center; align-items: center; display: flex;">
                                  <div class="material-switch pull-right">
                                    <input id="check{{$item->id}}" onchange="toggleChange('{{$item->id}}');" @if( @$item->status == '1') checked  @endif type="checkbox"/>
                                    <label for="check{{$item->id}}" class="label-success"></label>
                                  </div>

                                  <!-- @if( @$item->status == '1') Đang diễn ra @else Đã kết thuc @endif -->
                                </td>
                                <td class="text-center align-middle">
                                  <div class="btn-group align-top">
                                    <a class="btn btn-sm btn-primary badge" href="/admin/edit-events/{{$item->id}}" ><i class="mdi mdi-account-edit"></i>  </a>
                                    <button class="btn btn-sm btn-primary badge delete_conform" id="{{$item->id}}" type="button"><i class="fa fa-trash"></i></button>
                                  </div>
                                </td>

                              </tr>
                         @endforeach


                        </tbody>
                      </table>
                    </div>

                    {{ $users->appends(request()->query())->links() }}

                  </div>

                </div>
							</div>
						</div>


        </div>
      </div>

    </div>

  </div>
  <!--footer-->


  @include('admin.component.footer')

  <!-- End Footer-->
</div>
@endsection

@section('page-js')
    <!-- <script src="{{ asset('admins/assets/js/index3.js') }}"></script> -->
    <!-- <script src="{{ asset('admins/assets/js/index3-worldmap.js') }}"></script> -->
    <link href="{{ asset('admins/assets/plugins/sweet-alert/jquery.sweet-modal.min.css') }}" rel="stylesheet" />
		<link href="{{ asset('admins/assets/plugins/sweet-alert/sweetalert.css') }}" rel="stylesheet" />
    <script src="{{ asset('admins/assets/plugins/sweet-alert/jquery.sweet-modal.min.js') }}"></script>
    <script src="{{ asset('admins/assets/plugins/sweet-alert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('admins/assets/js/sweet-alert.js') }}"></script>
    <meta name="csrf" value="{{ csrf_token() }}">
    <script>
        function toggleChange(id){
          $.ajax({
             url: "/admin/edit-events-status/"+id,
             type: "post",
             contentType: "application/json; charset=utf-8",
             dataType: "json",
             headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              } ,
             success: function (response) {

               console.log('here');
               $(".update_status").attr("style","display: block;");
             }
         });


        }
        $( document ).ready(function() {
            $('.btn_search').on("click",function(){
                var name = $("#name").val();
                var status = $("#status").val();
                $("#name_val").val(name);
                $("#status_val").val(status);
                $("#form_search").submit();
            });
            $(".delete_conform").on("click",function(){
                var id = $(this).attr('id');
                swal({
            			title: "Xác nhận",
            			text: "Bạn đồng ý xoá sự kiện này ?",
            			type: "warning",
            			showCancelButton: true,
            			confirmButtonText: 'Có',
            			cancelButtonText: 'Không'
            		}, function(inputValue) {
                    if(inputValue){
                      var url= "/admin/delete-events/"+id;
                      window.location = url;
                    }

            		});
            });

        });
    </script>
@endsection
