@extends('layouts.admin_main')

@section('content')
<div class="content-area">
  <div class="container">

    <div class="page-header">
      <h4 class="page-title">Sự Kiện</h4>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/admin">Trang chủ</a></li>
        <li class="breadcrumb-item"><a href="/admin/users">Quản Lý Sự Kiện</a></li>
        <li class="breadcrumb-item active" aria-current="page">Thêm Sự Kiện</li>
      </ol>
    </div>
    <div class="row row-cards">
      <div class="col-lg-12">
          <form action="" method="post"  enctype="multipart/form-data">
            @csrf
            <div class="card">
              <div class="card-header">
                <h3 class="mb-0 card-title">Thông tin sự kiện</h3>
              </div>
              <div class="card-body">

                @if(@$message != '')
                <div class="alert alert-danger" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <p>- {{$message}}</p>
                </div>
                @endif

                @if(@$errors->all())
                <div class="alert alert-danger" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  @foreach ($errors->all() as $item)
                  <p>- {{$item}}</p>
                  @endforeach
                </div>
                @endif

                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" required class="form-control" name="data[name]" placeholder="Tên sự kiện" value="{{@$data['name']}}">
                    </div>
                    <div class="form-group">
                      <input type="file" required class="form-control" name="file_icon" placeholder="Hình ảnh" value="" >
                    </div>
                    <div class="form-group">
                      <select name="data[status]" required id="select-countries" class="form-control custom-select">

                        <option value="1" @if(@$data['status'] == 1) selected @endif>Đang diễn ra</option>
                        <option value="2" @if(@$data['status'] == 2) selected @endif>Kết thúc</option>
											</select>
                    </div>

                    <div class="form-group">
                      <select name="data[type]" required id="select-countries" class="form-control custom-select">
                        <option value="1" @if(@$data['type'] == 1) selected @endif>Trả Thưởng Theo Giải</option>
                        <option value="2" @if(@$data['type'] == 2) selected @endif>Trả Thưởng Toàn Giải</option>
											</select>
                    </div>
                    <div class="form-group">
                      <input type="text" required class="form-control" name="data[percent]" placeholder="Tỉ lệ giải" value="{{@$data['percent']}}">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" required class="form-control" name="data[code]" placeholder="Code" value="{{@$data['code']}}">
                    </div>
                    <div class="form-group has-success">
                      <label class="form-label">Ngày bắt đầu</label>
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <div class="input-group-text">
                            <i class="fa fa-calendar tx-16 lh-0 op-6"></i>
                          </div>
                        </div>
                        <input class="form-control fc-datepicker" name="data[start_date]" placeholder="Ngày bắt đầu" value="{{@$data['start_date']}}" type="text">
                      </div>
                      <!-- <input type="text" required class="form-control" name="data[address]" placeholder="Ngày bắt đầu" value="{{@$data['address']}}"> -->
                    </div>
                    <div class="form-group has-success">
                      <label class="form-label">Ngày kết thúc</label>
                      <div class="input-group">

                        <div class="input-group-prepend">
                          <div class="input-group-text">
                            <i class="fa fa-calendar tx-16 lh-0 op-6"></i>
                          </div>
                        </div>
                        <input class="form-control fc-datepicker" name="data[expired_at]" placeholder="Ngày kết thúc" value="{{@$data['expired_at']}}" type="text">
                      </div>
                      <!-- <input type="text" required class="form-control" name="data[address]" placeholder="Ngày bắt đầu" value="{{@$data['address']}}"> -->
                    </div>



                  </div>

                </div>
              </div>
              <div class="card-footer text-right">
    						<button type="submit" class="btn btn-primary">Thêm sự kiện</button>
    					</div>

            </div>
        </form>
      </div>

    </div>

  </div>
  <!--footer-->


  @include('admin.component.footer')

  <!-- End Footer-->
</div>
@endsection

@section('page-js')
    <!-- <script src="{{ asset('admins/assets/js/index3.js') }}"></script> -->
    <!-- <script src="{{ asset('admins/assets/js/index3-worldmap.js') }}"></script> -->
    <link href="{{ asset('admins/assets/plugins/sweet-alert/jquery.sweet-modal.min.css') }}" rel="stylesheet" />
		<link href="{{ asset('admins/assets/plugins/sweet-alert/sweetalert.css') }}" rel="stylesheet" />
    <link href="{{ asset('admins/assets/plugins/date-picker/spectrum.css') }}" rel="stylesheet" />
    <script src="{{ asset('admins/assets/plugins/sweet-alert/jquery.sweet-modal.min.js') }}"></script>
    <script src="{{ asset('admins/assets/plugins/sweet-alert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('admins/assets/js/sweet-alert.js') }}"></script>

    <script src="{{ asset('admins/assets/plugins/date-picker/spectrum.js') }}"></script>
		<script src="{{ asset('admins/assets/plugins/date-picker/jquery-ui.js') }}"></script>
		<script src="{{ asset('admins/assets/plugins/input-mask/jquery.maskedinput.js') }}"></script>

    <script src="{{ asset('admins/assets/js/custom.js') }}"></script>

    <script>

        $( document ).ready(function() {
          $(function() {
            'use strict'
            // Datepicker
            $('.fc-datepicker').datepicker({
              showOtherMonths: true,
              selectOtherMonths: true,
              dateFormat: 'yy-mm-dd'
            });

          });
        });
    </script>
@endsection
