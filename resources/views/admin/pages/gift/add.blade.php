@extends('layouts.admin_main')

@section('content')
<div class="content-area">
  <div class="container">

    <div class="page-header">
      <h4 class="page-title">Quà Tặng</h4>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/admin">Trang chủ</a></li>
        <li class="breadcrumb-item"><a href="/admin/users">Quản Lý Quà Tặng</a></li>
        <li class="breadcrumb-item active" aria-current="page">Thêm Quà Tặng</li>
      </ol>
    </div>
    <div class="row row-cards">
      <div class="col-lg-12">
          <form action="" method="post"  enctype="multipart/form-data">
            @csrf
            <div class="card">
              <div class="card-header">
                <h3 class="mb-0 card-title">Thông tin quà tặng</h3>
              </div>
              <div class="card-body">

                @if(@$message != '')
                <div class="alert alert-danger" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <p>- {{$message}}</p>
                </div>
                @endif

                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" required class="form-control" name="data[name]" placeholder="Tên giải thưởng" value="{{@$data['name']}}">
                    </div>
                    <div class="form-group">
                      <input type="file" required class="form-control" name="file_icon" placeholder="Hình ảnh" value="" >
                    </div>
                    <div class="form-group">
                      <input type="text" required class="form-control" name="data[name_gift]" placeholder="Tên sản phẩm" value="{{@$data['name_gift']}}">
                    </div>
                  </div>
                  <div class="col-md-6">

                    <div class="form-group">
                      <select name="data[event_id]" required id="select-countries" class="form-control custom-select">
												<option value="" >Sự kiện</option>
                        @foreach ($events as $item)
                            <option value="{{$item->id}}" @if(@$data['event_id'] == $item->id ) selected @endif >{{$item->name}}</option>
                        @endforeach
											</select>
                    </div>
                    
                    <div class="form-group">
                      <select name="data[status]" required id="select-countries" class="form-control custom-select">

                        <option value="1" @if(@$data['status'] == 1) selected @endif>Đang diễn ra</option>
                        <option value="2" @if(@$data['status'] == 2) selected @endif>Kết thúc</option>
											</select>
                    </div>

                    <div class="form-group">
                      <input type="text" required class="form-control" name="data[count]" placeholder="Số lượng giải" value="{{@$data['count']}}">
                    </div>



                  </div>

                </div>
              </div>
              <div class="card-footer text-right">
    						<button type="submit" class="btn btn-primary">Thêm</button>
    					</div>

            </div>
        </form>
      </div>

    </div>

  </div>
  <!--footer-->


  @include('admin.component.footer')

  <!-- End Footer-->
</div>
@endsection

@section('page-js')
    <!-- <script src="{{ asset('admins/assets/js/index3.js') }}"></script> -->
    <!-- <script src="{{ asset('admins/assets/js/index3-worldmap.js') }}"></script> -->
    <link href="{{ asset('admins/assets/plugins/sweet-alert/jquery.sweet-modal.min.css') }}" rel="stylesheet" />
		<link href="{{ asset('admins/assets/plugins/sweet-alert/sweetalert.css') }}" rel="stylesheet" />
    <link href="{{ asset('admins/assets/plugins/date-picker/spectrum.css') }}" rel="stylesheet" />
    <script src="{{ asset('admins/assets/plugins/sweet-alert/jquery.sweet-modal.min.js') }}"></script>
    <script src="{{ asset('admins/assets/plugins/sweet-alert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('admins/assets/js/sweet-alert.js') }}"></script>

    <script src="{{ asset('admins/assets/plugins/date-picker/spectrum.js') }}"></script>
		<script src="{{ asset('admins/assets/plugins/date-picker/jquery-ui.js') }}"></script>
		<script src="{{ asset('admins/assets/plugins/input-mask/jquery.maskedinput.js') }}"></script>

    <script src="{{ asset('admins/assets/js/custom.js') }}"></script>

    <script>

        $( document ).ready(function() {
          $(function() {
            'use strict'
            // Datepicker
            $('.fc-datepicker').datepicker({
              showOtherMonths: true,
              selectOtherMonths: true,
              dateFormat: 'yy-mm-dd'
            });

          });
        });
    </script>
@endsection
