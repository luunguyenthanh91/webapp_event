@extends('layouts.admin_main')

@section('content')
<div class="content-area">
  <div class="container">

    <div class="page-header">
      <h4 class="page-title">Giải thưởng</h4>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/admin">Trang Chủ</a></li>
        <li class="breadcrumb-item active" aria-current="page">Quản Lý Giải Thưởng</li>
      </ol>
    </div>
    <div class="row row-cards">
      <div class="col-lg-12">
        <div>

          <div class="row row-cards">
							<div class="col-lg-3">
								<div class="row">
									<div class="col-md-12 col-lg-12">
										<div class="card">
											<div class="card-header">
												<div class="card-title"> Điều Kiện Lọc</div>
											</div>
											<div class="card-body">
												<div class="form-group">
													<label class="form-label">Sự Kiện</label>
													<select id="event_id" name="condition[event_id]" class="form-control custom-select">
														<option value="">--Tất Cả--</option>
                            @foreach ($events as $item)
                                <option value="{{$item->id}}" @if(@$condition['event_id'] == $item->id ) selected @endif >{{$item->name}}</option>
                            @endforeach
													</select>
                          <button type="button" class="btn btn-primary btn_search col-md-12 col-lg-12  mt-3">
                            <i class="fa fa-search " aria-hidden="true"></i>
                          </button>
												</div>



                        <div class="form-group">
                            <label class="form-label">Thêm Giải Thưởng</label>
        										<a href="/admin/add-gift" class="btn btn-primary  col-md-12 col-lg-12">
        											<i class="fa fa-calendar-plus-o " aria-hidden="true"></i>
        										</a>
												</div>


                        <form method="post" action="" id="form_search"  >
                            @csrf
                            <input type="hidden" name="condition[event_id]" id="event_id_val" />
                        </form>


											</div>
										</div>

									</div>
								</div>
							</div>

							<div class="col-lg-9">
                <div class="update_status" style="display:none">
                  <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                      Thay đổi tình trạng thành công.
                  </div>
                </div>
								<div class="card store">
                  @if($message != '')
                  <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{$message}}
                  </div>
                  @endif
                  <div class="e-table">
                    <div class="table-responsive table-lg">
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th  class="text-center">
                                #
                            </th>
                            <th class="text-center">Tên sự kiện</th>
                            <th>Giải thưởng</th>
                            <th>Số lượng còn lại</th>
                            <th>Vật phẩm thưởng</th>
                            <th>Tình trạng</th>
                            <th class="text-center"></th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach ($users as $item)
                              <tr>
                                <td class="align-middle text-center">
                                    {{$item->id}}
                                </td>
                                <td class="align-middle text-center">{{$item['event']['name']}}</td>
                                <td class="text-center align-middle">{{$item->name}}</td>
                                <td class="text-center align-middle">{{$item->count}}</td>
                                <td class="text-center align-middle">{{$item->name_gift}}</td>

                                <td class="text-center align-middle">{{$item->name_gift}}</td>
                                <td class="align-middle" style="justify-content: center; align-items: center; display: flex;">
                                  <div class="material-switch pull-right">
                                    <input id="check{{$item->id}}" onchange="toggleChange('{{$item->id}}');" @if( @$item->status == '1') checked  @endif type="checkbox"/>
                                    <label for="check{{$item->id}}" class="label-success"></label>
                                  </div>

                                  <!-- @if( @$item->status == '1') Đang diễn ra @else Đã kết thuc @endif -->
                                </td>
                                <td class="text-center align-middle">
                                  <div class="btn-group align-top">
                                    <a class="btn btn-sm btn-primary badge" href="/admin/edit-gift/{{$item->id}}" ><i class="mdi mdi-account-edit"></i>  </a>
                                    <button class="btn btn-sm btn-primary badge delete_conform" id="{{$item->id}}" type="button"><i class="fa fa-trash"></i></button>
                                  </div>
                                </td>

                              </tr>
                         @endforeach


                        </tbody>
                      </table>
                    </div>

                    {{ $users->appends(request()->query())->links() }}

                  </div>

                </div>
							</div>
						</div>


        </div>
      </div>

    </div>

  </div>
  <!--footer-->


  @include('admin.component.footer')

  <!-- End Footer-->
</div>
@endsection

@section('page-js')
    <!-- <script src="{{ asset('admins/assets/js/index3.js') }}"></script> -->
    <!-- <script src="{{ asset('admins/assets/js/index3-worldmap.js') }}"></script> -->
    <link href="{{ asset('admins/assets/plugins/sweet-alert/jquery.sweet-modal.min.css') }}" rel="stylesheet" />
		<link href="{{ asset('admins/assets/plugins/sweet-alert/sweetalert.css') }}" rel="stylesheet" />
    <script src="{{ asset('admins/assets/plugins/sweet-alert/jquery.sweet-modal.min.js') }}"></script>
    <script src="{{ asset('admins/assets/plugins/sweet-alert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('admins/assets/js/sweet-alert.js') }}"></script>
    <script>
        function toggleChange(id){
          $.ajax({
             url: "/admin/edit-gift-status/"+id,
             type: "post",
             contentType: "application/json; charset=utf-8",
             dataType: "json",
             headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              } ,
             success: function (response) {
               console.log('here');
               $(".update_status").attr("style","display: block;");
             }
         });


        }
        $( document ).ready(function() {
            $('.btn_search').on("click",function(){
                var event_id = $("#event_id").val();
                $("#event_id_val").val(event_id);
                $("#form_search").submit();
            });
            $(".delete_conform").on("click",function(){
                var id = $(this).attr('id');
                swal({
            			title: "Xác nhận",
            			text: "Bạn đồng ý xoá giải thưởng này ?",
            			type: "warning",
            			showCancelButton: true,
            			confirmButtonText: 'Có',
            			cancelButtonText: 'Không'
            		}, function(inputValue) {
                    if(inputValue){
                      var url= "/admin/delete-gift/"+id;
                      window.location = url;
                    }

            		});
            });
        });
    </script>
@endsection
