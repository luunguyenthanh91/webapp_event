@extends('layouts.admin_main')

@section('content')
<div class="content-area">
  <div class="container">
    <div class="page-header">
      <h4 class="page-title">Dashboard</h4>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Gaming Dashboard</li>
      </ol>
    </div>

    <div class="row row-cards">
      <div class="col-lg-6 col-xl-3 col-md-6 col-sm-12">
        <div class="card text-center">
          <div class="card-body text-center">
                                <h5 class="mb-5">Total Tournaments</h5>
              <span class="bar" data-peity='{ "fill": ["#ff695c", "#ff4f7b"]}'>6,2,8,4,3,8,1,3,6,5,9,2,8,1,4,8,9,8,2,1</span>
            </div>
        </div>
      </div>
      <div class="col-lg-6 col-xl-3 col-md-6 col-sm-12">
        <div class="card text-center">
          <div class="card-body text-center">
                                <h5 class="mb-5">Total Participations</h5>
              <span class="bar" data-peity='{ "fill": ["#32cafe ", "#3582ec"]}'>5,3,9,6,5,9,7,3,5,2,5,3,9,6,5,9,3,7,2,9</span>
            </div>
        </div>
      </div>
      <div class="col-lg-6 col-xl-3 col-md-6 col-sm-12">
        <div class="card text-center">
          <div class="card-body text-center ">
                                <h5 class="mb-5">Contribution</h5>
              <span class="bar" data-peity='{ "fill": ["#ecd53e", "#efaf28"]}'>3,7,9,4,2,8,4,6,4,9,2,3,9,4,1,7,3,9,8,4,5</span>
            </div>
        </div>
      </div>
      <div class="col-lg-6 col-xl-3 col-md-6 col-sm-12">
        <div class="card text-center">
          <div class="card-body text-center">
                                <h5 class="mb-5">Weekly Players</h5>
              <span class="bar" data-peity='{ "fill": ["#63d457", "#3cbf2d"]}'>2,7,3,9,4,5,2,8,4,6,5,2,8,4,7,2,4,6,4,8,4</span>
            </div>
        </div>
      </div>
    </div>
                <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-6">
        <div class="card overflow-hidden">
          <div class="card-header">
            <h3 class="card-title">Adoption Participations</h3>
          </div>
          <div class="">
            <div class="card-body">
              <h5>Total Number of participants in the Games</h5>
              <h3 class="mb-1 text-primary">56,7824</h3>
              <small class="displayblock"><i class="mdi mdi-arrow-up-drop-circle mr-1 text-success" aria-hidden="true"></i>21% Increase in this Month </small>
              <div class="row mt-6 text-center">
                <div class="col-md-4 col-sm-12 col-xl-6 col-lg-6 ">
                  <span class="dot-label bg-primary"></span><span class="mr-3">Active Users</span>
                </div>
                <div class="col-md-4 col-sm-12 col-xl-6 col-lg-6 ">
                  <span class="dot-label bg-secondary"></span><span class="mr-3">Contributing</span>
                </div>
              </div>
            </div>
            <div class="chart-wrapper ">
                <canvas id="widgetChart1" class="mb-0 p-0 chart-dropshadow"></canvas>
              </div>
          </div>
        </div>
      </div>

        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-6">
        <div class="row">
          <div class="col-sm-12 col-lg-6 col-md-6 ">
            <div class="card card-counter text-white bg-gradient-secondary shadow-secondary">
              <div class="card-body">
                <div class="card-value float-right text-purple">
                  <i class="fa fa-signal  mb-0"></i>
                </div>
                <h3 class="mb-1 counter font-30">$751,258</h3>
                <div class="text-white">Revenue</div>
              </div>
            </div>
          </div>

          <div class="col-sm-12 col-lg-6 col-md-6">
            <div class="card bg-gradient-primary shadow-primary card-counter text-white">
              <div class="card-body">
                <div class="card-value float-right ">
                  <i class="fa fa-cubes  mb-0"></i>
                </div>
                <h3 class="mb-1 counter font-30">985,399</h3>
                <div class="">Price Won</div>
              </div>
            </div>
          </div>
          <div class="col-sm-12 col-lg-12 col-md-12 col-xl-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Desktop / Tablet / Mobile Ratio</h3>
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="col-xl-4 col-sm-12 col-md-12 col-lg-4 text-center ratio mb-3">
                    <div class="chart-circle mt-4 mt-sm-0 mb-0" data-value="0.33" data-thickness="8" data-color="#ff695c"><canvas width="128" height="128"></canvas>
                      <div class="chart-circle-value text-center ">30%<small>Desktop</small></div>
                    </div>
                  </div>
                  <div class="col-xl-4 col-sm-12 col-md-12 col-lg-4 mb-3 ratio">
                    <div class="chart-circle mt-4 mt-sm-0 mb-0" data-value="0.55" data-thickness="8" data-color="#32cafe"><canvas width="128" height="128"></canvas>
                      <div class="chart-circle-value">50%<small>Tablet</small></div>
                    </div>
                  </div>
                  <div class="col-xl-4 col-sm-12 col-md-12 col-lg-4 mb-2 ratio">
                    <div class="chart-circle mt-4 mt-sm-0 mb-0" data-value="0.88" data-thickness="8" data-color="#5ed84f "><canvas width="128" height="128"></canvas>
                      <div class="chart-circle-value">80% <small>Mobiles</small></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        </div>
    </div>

    <div class="row row-deck">
      <div class="col-xs-12 col-sm-12 col-lg-6 col-xl-6">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Compaign Graph</h3>
          </div>
          <div class="card-body">
            <div id="echart1" class="chartsh chart-dropshadow mb-2"></div>
            <div class="row mt-6 text-center">
                <div class="col-md-4 col-sm-12 col-lg-4 ">
                <span class="dot-label bg-primary"></span><span class="">Installs</span>
                </div>
              <div class="col-md-4 col-sm-12 col-lg-4 mr-0 ml-0 ">
                <span class="dot-label bg-secondary"></span><span class="">Impressions</span>
                </div>
               <div class="col-md-4 col-sm-12 col-lg-4 ">
                <span class="dot-label bg-success"></span><span class="">Clicks</span>
               </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-lg-6 col-xl-6">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Daily Active Users</h3>
          </div>
          <div class="">
            <div class="card-body">
              <div class="chart-wrapper ">
                    <div id="placeholder4" class="chartsh"></div>
                  </div>
              <div class="row mt-3 text-center">
                 <div class="col-md-6 col-sm-12 col-xl6 col-lg-6 sales">
                      <h3 class="mb-0">12,526</h3>
                  <small class="mt-2">Today Users</small>
                 </div>
                  <div class="col-md-6 col-sm-12 col-xl6 col-lg-6 ">
                    <h3 class="mb-1">80,3452</h3>
                    <span class="dot-label bg-primary"></span><span class="mr-3">Daily Active Users</span>
                 </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row row-cards">
      <div class="col-xs-12 col-sm-12 col-lg-12 col-xl-12">
        <div class="card">
            <div class="card-header">
            <h3 class="card-title">Top Country Players</h3>
          </div>
          <div class="row pl-0 pr-0 ml-0 mr-0">
              <div class="col-xl-8 worldmap pl-0 pr-0 ml-0 mr-0 ">
              <div class="card-body">
                <div id="world-map-markers" class="worldh h-400 "></div>
              </div>
            </div>
            <div class="col-xl-4 pl-0">
              <div class="card-body">
                  <h3 class="card-title mb-4 mt-3">Top 5 Active Countries</h3>
                <div class="progress-wrapper pt-2">
                  <div class="mb-3">
                    <p class="mb-2">Monaco<span class="float-right text-muted">80%</span></p>
                    <div class="progress h-2">
                        <div class="progress-bar bg-gradient-primary w-80" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                      </div>
                  </div>
                </div>
                <div class="progress-wrapper pt-2">
                  <div class="mb-3">
                    <p class="mb-2">Singapore<span class="float-right text-muted">45%</span></p>
                    <div class="progress h-2">
                      <div class="progress-bar bg-gradient-secondary w-45" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                  </div>
                </div>
                <div class="progress-wrapper pt-2">
                    <div class="mb-3">
                    <p class="mb-2">Dominica<span class="float-right text-muted">60%</span></p>
                    <div class="progress h-2">
                      <div class="progress-bar bg-gradient-warning w-60" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                  </div>
                </div>
                <div class="progress-wrapper pt-2">
                    <div class="mb-3">
                    <p class="mb-2">Bahrain<span class="float-right text-muted">56%</span></p>
                    <div class="progress h-2">
                      <div class="progress-bar bg-gradient-success w-55" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                  </div>
                </div>
                <div class="progress-wrapper pt-2">
                    <div class="mb-3">
                    <p class="mb-2">Tuvalu<span class="float-right text-muted">30%</span></p>
                    <div class="progress h-2">
                      <div class="progress-bar bg-gradient-purple w-30" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                  </div>
                </div>
                <div class="progress-wrapper pt-2">
                    <div class="mb-3">
                    <p class="mb-2">Maldives<span class="float-right text-muted">58%</span></p>
                    <div class="progress h-2">
                      <div class="progress-bar bg-gradient-info w-60" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--footer-->


  @include('admin.component.footer')

  <!-- End Footer-->
</div>
@endsection

@section('page-js')
    <script src="{{ asset('admins/assets/js/index3.js') }}"></script>
    <script src="{{ asset('admins/assets/js/index3-worldmap.js') }}"></script>
@endsection
