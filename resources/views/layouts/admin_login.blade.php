<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="UTF-8">
        <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <meta name="msapplication-TileColor" content="#ff685c">
        <meta name="theme-color" content="#32cafe">
        <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="mobile-web-app-capable" content="yes">
        <meta name="HandheldFriendly" content="True">
        <meta name="MobileOptimized" content="320">
        <link rel="icon" href="{{ asset('admins/assets/favicon.ico') }}" type="image/x-icon"/>
        <link rel="shortcut icon" type="image/x-icon" href="{{ asset('admins/assets/favicon.ico') }}" />
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Title -->
        <title>Admin Event Lucky Draw</title>
        <link rel="stylesheet" href="{{ asset('admins/assets/fonts/fonts/font-awesome.min.css') }}">

    		<!-- Font family -->
    		<link href="https://fonts.googleapis.com/css?family=Comfortaa:300,400,700" rel="stylesheet">

    		<!-- Dashboard Css -->
    		<link href="{{ asset('admins/assets/css/dashboard.css') }}" rel="stylesheet" />

    		<!-- c3.js Charts Plugin -->
    		<link href="{{ asset('admins/assets/plugins/charts-c3/c3-chart.css') }}" rel="stylesheet" />

    		<!-- Custom scroll bar css-->
    		<link href="{{ asset('admins/assets/plugins/scroll-bar/jquery.mCustomScrollbar.css') }}" rel="stylesheet" />

    		<!-- Horizontal-menu Css -->
    		<link href="{{ asset('admins/assets/plugins/horizontal-menu/dropdown-effects/fade-down.css') }}" rel="stylesheet">
    		<link href="{{ asset('admins/assets/plugins/horizontal-menu/webslidemenu.css') }}" rel="stylesheet">

    		<!-- jvectormap CSS -->
        <link href="{{ asset('admins/assets/plugins/jvectormap/jquery-jvectormap-2.0.2.css') }}" rel="stylesheet" />

    		<!---Font icons-->
    		<link href="{{ asset('admins/assets/plugins/iconfonts/plugin.css') }}" rel="stylesheet" />


    </head>
    <body class="login-img" >

        <div id="global-loader" ></div>
        <div class="page">

            @yield('content')
        </div>
        <a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

    		<!-- Dashboard Core -->
    		<script src="{{ asset('admins/assets/js/vendors/jquery-3.2.1.min.js') }}"></script>
    		<script src="{{ asset('admins/assets/js/vendors/bootstrap.bundle.min.js') }}"></script>
    		<script src="{{ asset('admins/assets/js/vendors/jquery.sparkline.min.js') }}"></script>
    		<script src="{{ asset('admins/assets/js/vendors/selectize.min.js') }}"></script>
    		<script src="{{ asset('admins/assets/js/vendors/jquery.tablesorter.min.js') }}"></script>
    		<script src="{{ asset('admins/assets/js/vendors/circle-progress.min.js') }}"></script>
    		<script src="{{ asset('admins/assets/plugins/rating/jquery.rating-stars.js') }}"></script>
    		<script src="{{ asset('admins/assets/plugins/flot/jquery.flot.js') }}"></script>
    		<script src="{{ asset('admins/assets/plugins/flot/jquery.flot.fillbetween.js') }}"></script>
    		<script src="{{ asset('admins/assets/plugins/flot/jquery.flot.pie.js') }}"></script>

    		<!--Horizontal-menu Js-->
    		<script src="{{ asset('admins/assets/plugins/horizontal-menu/webslidemenu.js') }}"></script>

    		<!--Jquery.knob js-->
    		<script src="{{ asset('admins/assets/plugins/othercharts/jquery.knob.js') }}"></script>

    		<!--othercharts js-->
    		<script src="{{ asset('admins/assets/js/othercharts.js') }}"></script>

    		<!-- Charts Plugin -->
    		<script src="{{ asset('admins/assets/plugins/chart/Chart.bundle.js') }}"></script>
    		<script src="{{ asset('admins/assets/plugins/chart/utils.js') }}"></script>

    		<!-- peitychart -->
    		<script src="{{ asset('admins/assets/plugins/peitychart/jquery.peity.min.js') }}"></script>
    		<script src="{{ asset('admins/assets/plugins/peitychart/peitychart.init.js') }}"></script>

    		<!-- Input Mask Plugin -->
    		<script src="{{ asset('admins/assets/plugins/input-mask/jquery.mask.min.js') }}"></script>

    		<!-- Custom scroll bar Js-->
    		<script src="{{ asset('admins/assets/plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js') }}"></script>

        <!-- Index Scripts -->
    		<!-- <script src="{{ asset('admins/assets/js/index3.js') }}"></script> -->

    		<!-- Search Js-->
    		<script src="{{ asset('admins/assets/js/prefixfree.min.js') }}"></script>

    		<!-- ECharts Plugin -->
  	    <script src="{{ asset('admins/assets/plugins/echarts/echarts.js') }}"></script>

    		<!-- Vector Plugin -->
    		<script src="{{ asset('admins/assets/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js') }}"></script>
        <script src="{{ asset('admins/assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>

    		<!-- Custom-->
    		<script src="{{ asset('admins/assets/js/custom.js') }}"></script>
    		<!-- <script src="{{ asset('admins/assets/js/index3-worldmap.js') }}"></script> -->

        @yield('page-js')

    </body>
</html>
