<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['namespace' => 'API\V1'], function() {
    Route::post('/register','Auth\AuthController@register');
    Route::post('/login','Auth\AuthController@login');
    Route::post('/facebook','Auth\AuthController@facebook');
    Route::post('/login-msnv','Auth\AuthController@msnv');
    Route::post('/check-event','EventController@check');
    Route::post('/check-event-gift','EventController@checkGift');
    Route::post('/run-event','EventController@runAction');
    Route::post('/update-event-gift','EventController@updateGift');
    Route::post('/history-gift','EventController@historyGift');
});

// Route::group(['namespace' => 'API\V1'], function() {
//     Route::group(['prefix' => 'auth'],function(){
//         Route::post('/register','Auth\AuthController@register');
//         Route::post('/login','Auth\AuthController@login');
//         Route::post('/facebook','Auth\AuthController@facebook');
//         Route::post('/google','Auth\AuthController@google');
//
//     });
//     Route::group(['prefix' => 'auth','middleware' => 'auth:api'],function(){
//         Route::post('/logout','Auth\AuthController@logout');
//         Route::post('/update-password','Auth\AuthController@updatePassword');
//         Route::post('/update','Auth\AuthController@update');
//
//     });
//
//     Route::get('/home','HomeController@home');
//     Route::get('/category/list','CategoryController@getList');
//
//
// });
//
// // Luu Nguyen Control Api Start
// Route::group(['namespace' => 'API\V2'], function() {
//
//     // Auction create, update, delete and sold
//     Route::group(['prefix' => 'auctions' , 'middleware' => 'auth:api'],function(){
//         Route::post('/', 'AuctionController@store');
//         Route::post('/user', 'AuctionController@getAuctionByUser');
//     });
//
//     // Auction search , detail
//     Route::group(['prefix' => 'auctions'],function(){
//         Route::get('/', 'AuctionController@index');
//         Route::get('/{id}', 'AuctionController@show');
//     });
//
//     // Auction place bid
//     Route::group(['prefix' => 'bids','middleware' => 'auth:api'],function(){
//         Route::post('/{id}', 'BidsController@store');
//         Route::get('/user/', 'BidsController@getBidByUser');
//         Route::get('/user/{id}', 'BidsController@getBidByUserDetail');
//     });
//
//     // Get bids
//     Route::group(['prefix' => 'bids'],function(){
//         // Route::get('/{id}', 'BidsController@show');
//     });
//
//     Route::post('test-push','BidsController@testPush');
// });
