<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/**
 * @project: LaraBid
 * @website: https://themeqx.com
 */

Auth::routes();
Route::get('/export','Admin\UserController@export');
//Dashboard Route
Route::group(['prefix'=>'admin', 'middleware' => 'admin'], function(){
      // Authencation and home page in admin
      Route::get('/','Admin\HomeController@index');
      Route::get('/logout','Admin\HomeController@logout');

      // Action in admin User
      Route::get('/users','Admin\UserController@index');
      Route::post('/users','Admin\UserController@index');
      Route::get('/delete-user/{id}','Admin\UserController@delete');
      Route::get('/add-user','Admin\UserController@add');
      Route::post('/add-user','Admin\UserController@add');
      Route::get('/edit-user/{id}','Admin\UserController@edit');
      Route::post('/edit-user/{id}','Admin\UserController@edit');

      Route::post('/import-excel','Admin\UserController@import');

      // Action Event
      Route::get('/events','Admin\EventsController@index');
      Route::post('/events','Admin\EventsController@index');
      Route::get('/delete-events/{id}','Admin\EventsController@delete');
      Route::get('/add-events','Admin\EventsController@add');
      Route::post('/add-events','Admin\EventsController@add');
      Route::get('/edit-events/{id}','Admin\EventsController@edit');
      Route::post('/edit-events/{id}','Admin\EventsController@edit');

      Route::post('/edit-events-status/{id}','Admin\EventsController@editAjax');

      // Action Gift
      Route::get('/gift','Admin\GiftController@index');
      Route::post('/gift','Admin\GiftController@index');
      Route::get('/delete-gift/{id}','Admin\GiftController@delete');
      Route::get('/add-gift','Admin\GiftController@add');
      Route::post('/add-gift','Admin\GiftController@add');
      Route::get('/edit-gift/{id}','Admin\GiftController@edit');
      Route::post('/edit-gift/{id}','Admin\GiftController@edit');

      Route::post('/edit-gift-status/{id}','Admin\GiftController@editAjax');
      // Action Gift
      Route::get('/history','Admin\HistoryController@index');
      Route::post('/history','Admin\HistoryController@index');
      Route::get('/delete-history/{id}','Admin\HistoryController@delete');


      Route::get('/history-win','Admin\HistoryController@indexWin');
      Route::post('/history-win','Admin\HistoryController@indexWin');
      Route::get('/delete-history-win/{id}','Admin\HistoryController@deleteWin');
});

Route::group(['prefix'=>'admin'], function(){
      Route::get('/login','Admin\UserController@login');
      Route::post('/login','Admin\UserController@login')->name('login_admin');
});
Route::group(['middleware' => 'admin'], function(){
    Route::get('/','Admin\HomeController@index');
});
