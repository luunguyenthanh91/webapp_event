<?php

namespace App\Http\Controllers\Admin;

use App\Event;
use App\Gift;
use App\History;
use App\Country;
use App\Favorite;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Session;
use Excel;

class HistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // protected $redirectTo = '/dashboard';


    public function deleteWin(Request $request,$id){
        $user = History::where('id',$id)->first();
        try {
            if($user){
              try {
                  if($user->status == 2){
                      $count = History::where('gift_id','=' ,$user->gift_id)->count();
                      $gift = Gift::find($user->gift_id);
                      $gift->list_number = $gift->list_number .($count+10) . ',';
                      $gift->count = $gift->count + 1 ;
                      $gift->save();
                  }
              } catch (\Exception $e) {
                  // print_r($e->getMessage());die;
              }
              $user->delete();
              Session::put('message', "Xoá lịch sử thành công.");
            }
            return redirect('/admin/history-win');
        } catch (\Exception $e) {
            print_r($e->getMessage());die;
        }



    }


    public function delete(Request $request,$id){
        $user = History::where('id',$id)->first();
        try {
          if($user){
            try {
                if($user->status == 2){
                    $count = History::where('gift_id','=' ,$user->gift_id)->count();
                    $gift = Gift::find($user->gift_id);
                    $gift->list_number = $gift->list_number .($count+10) . ',';
                    $gift->count = $gift->count + 1 ;
                    $gift->save();
                }
            } catch (\Exception $e) {
                // print_r($e->getMessage());die;
            }
            $user->delete();
            Session::put('message', "Xoá lịch sử thành công.");
          }


          return redirect('/admin/history');
        } catch (\Exception $e) {
            print_r($e->getMessage());die;
        }
    }

    public function indexWin(Request $request){
        $menu = 'historys_gift';
        $events = Event::orderby('id','DESC');
        $events = $events->where('status','=',1);
        $events = $events->get();

        if(!$request->page){
            $page = 1;
        }else{
            $page = $request->page;
        }
        $users = History::orderby('id','DESC');
        // $users = $users->join('event','gift.event_id','=','event.id');
        if ($request->isMethod('post')) {
            $condition = @$request->condition;
            Session::put('historys_condition', $condition);
        }
        else{
            if(Session::get('historys_condition')){
                $condition = Session::get('historys_condition');
            }
        }

        if(@$condition['event_id'] != ''){
            $users = $users->where('event_id','=',$condition['event_id']);
        }
        $users = $users->where('status','=',2);


        $users = $users->paginate(20);

        foreach ($users as $key => $value) {
          $users[$key]['event'] = Event::find($value['event_id']);
          $users[$key]['users'] = User::find($value['user_id']);
          $users[$key]['gift'] = Gift::find($value['gift_id']);
        }

        // echo "<pre>";
        // print_r($users);
        // echo "</pre>";
        // die;
        if(Session::get('message')){
            $message = Session::get('message');
            Session::put('message', "");
        }else{
            $message = "";
        }

        return view('admin.pages.history.managerWin',compact('menu', 'events' ,'users','page' , 'condition' , 'message'));
    }

    public function index(Request $request){
        $menu = 'historys';
        $events = Event::orderby('id','DESC');
        $events = $events->where('status','=',1);
        $events = $events->get();

        if(!$request->page){
            $page = 1;
        }else{
            $page = $request->page;
        }
        $users = History::orderby('id','DESC');
        // $users = $users->join('event','gift.event_id','=','event.id');
        if ($request->isMethod('post')) {
            $condition = @$request->condition;
            Session::put('historys_condition', $condition);
        }
        else{
            if(Session::get('historys_condition')){
                $condition = Session::get('historys_condition');
            }
        }

        if(@$condition['event_id'] != ''){
            $users = $users->where('event_id','=',$condition['event_id']);
        }



        $users = $users->paginate(20);

        foreach ($users as $key => $value) {
          $users[$key]['event'] = Event::find($value['event_id']);
          $users[$key]['users'] = User::find($value['user_id']);
          $users[$key]['gift'] = Gift::find($value['gift_id']);
        }

        // echo "<pre>";
        // print_r($users);
        // echo "</pre>";
        // die;
        if(Session::get('message')){
            $message = Session::get('message');
            Session::put('message', "");
        }else{
            $message = "";
        }

        return view('admin.pages.history.manager',compact('menu', 'events' ,'users','page' , 'condition' , 'message'));
    }





}
