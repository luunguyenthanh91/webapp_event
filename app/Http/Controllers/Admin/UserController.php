<?php

namespace App\Http\Controllers\Admin;
use App\Ad;
use App\Country;
use App\Favorite;
use App\User;

use App\Event;
use App\Gift;
use App\History;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Session;
use Excel;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $redirectTo = '/dashboard';

    public function export(Request $request)
    {
      $allUser = History::orderby('id','DESC');
      $allUser = $allUser->join('event','history_event.event_id','=','event.id');
      $allUser = $allUser->join('users','history_event.user_id','=','users.id');
      $allUser = $allUser->join('gift','history_event.gift_id','=','gift.id');
      $allUser = $allUser->where("history_event.status","=", 2);
      // print_r($allUser);die;
      $allUser = $allUser->get();
      // print_r($allUser);die;
      $data = [];
      foreach ($allUser as $key => $value) {
        $data[] = ['gift'=>$value->name, "user" => $value->email] ;
      }
      // print_r($data);die;

        $header = []; //headers
        $excel = Excel::download($allUser, "excel.csv");
        return $excel;
    }

    public function import(Request $request)
    {
      if($request->hasFile('file_excel')){
          $path = $request->file('file_excel');
          $destinationPath = 'uploads/file/';
          $path->move($destinationPath,$path->getClientOriginalName());
          $name = $destinationPath.$path->getClientOriginalName();

          $data = \Excel::toArray('', $name, null, \Maatwebsite\Excel\Excel::TSV)[0];
          foreach ($data as $key => $value) {
              $user = User::where('user_name',$value[0])->where('type_login',3)->first();
              if(!$user){
                  $user = new User();
                  $user->email = $value[0].'@gmail.com';
                  $user->user_name = $value[0];
                  $user->event_id = $value[1];
                  $user->type_login = 3;
                  $user->save();
              }
          }

       }
      Session::put('message', "Import dữ liệu thành công.");
      return redirect('/admin/users');

    }

    public function edit(Request $request,$id)
    {
        $menu = 'users';
        $message = '';
        if ($request->isMethod('post')) {
            $data = $request->data;
            $validator = Validator::make($data, [
                    'name' => 'required',
                    'phone' => 'required|numeric',
                    'address' => 'required',
                ],[
                  'name.required' => 'Họ tên không được để trống',
                  'phone.required' => 'Số điện thoại không được để trống',
                  'phone.numeric' => 'Số điện thoại phải là chữ số',
                  'address.required' => 'Địa chỉ không được để trống',
                ]
            );

            if ($validator->fails()) {
                $errors = $validator->errors();
                return view('admin.pages.edit_user', compact('data','menu','errors'));
            }

            if($data['password'] != $data['re_password'] && $data['password'] !='' ){
              $message = "Mật khẩu không trùng khớp với nhập lại mật khẩu";
              return view('admin.pages.edit_user', compact('data','menu','message'));
            }
            $user =  User::find($id);
            if($data['password'] != ''){
                $user->password = bcrypt($data['password']);
            }
            $user->name = $data['name'];
            $user->address = $data['address'];
            $user->phone = $data['phone'];
            $user->save();

            Session::put('message', "Thay đổi thông tin tài khoản thành công.");
            return redirect('/admin/users');
        }else{
          $data = User::find($id);
        }


        return view('admin.pages.edit_user', compact('data','menu','message'));
    }


    public function add(Request $request)
    {
        $menu = 'users';
        $message = '';
        if ($request->isMethod('post')) {
            $data = $request->data;
            $validator = Validator::make($data, [
                    'name' => 'required',
                    'email' => 'required',
                    'phone' => 'required|numeric',
                    'address' => 'required',
                    'password' => 'required',
                    're_password' => 'required'
                ],[
                  'name.required' => 'Họ tên không được để trống',
                  'email.required' => 'Email không được để trống',
                  'phone.required' => 'Số điện thoại không được để trống',
                  'phone.numeric' => 'Số điện thoại phải là chữ số',
                  'address.required' => 'Địa chỉ không được để trống',
                  'password.required' => 'Mật khẩu không được để trống',
                  're_password.required' => 'Nhập lại mật khẩu không được để trống'
                ]
            );

            if ($validator->fails()) {
                $errors = $validator->errors();
                return view('admin.pages.add_user', compact('data','menu','errors'));
            }
            $user = User::where('email',$data['email'])->where('type_login',1)->first();
            if($user){
                $message = "Email này đã được sử dụng để đăng ký website . Vui lòng thử lại email khác.";
                return view('admin.pages.add_user', compact('data','menu','message'));
            }
            if($data['password'] != $data['re_password']){
              $message = "Mật khẩu không trùng khớp với nhập lại mật khẩu";
              return view('admin.pages.add_user', compact('data','menu','message'));
            }

            $datainsert = [
                'name'        => $data['name'],
                'email'       => $data['email'],
                'address'     => $data['address'],
                'phone'       => $data['phone'],
                'password'    => bcrypt($data['password'])

            ];
            $user = User::create($datainsert);

            Session::put('message', "Thêm tài khoản thành công.");
            return redirect('/admin/users');
        }else{
          $data = [];
        }


        return view('admin.pages.add_user', compact('data','menu','message'));
    }


    public function delete(Request $request,$id){
        $user = User::where('id',$id)->first();
        if(@$user->role != 'admin'){
              $user->delete();
              Session::put('message', "Xoá tài khoản thành công.");
        }else{
            Session::put('message', "Tài khoản admin không được quyền xoá.");
        }


        return redirect('/admin/users');

    }

    public function index(Request $request){
        $menu = 'users';
        if(!$request->page){
          $page = 1;
        }else{
            $page = $request->page;
        }
        $users = User::orderby('id','DESC');
        if ($request->isMethod('post')) {
            $condition = @$request->condition;
            Session::put('user_condition', $condition);
        }
        else{
            if(Session::get('user_condition')){
                $condition = Session::get('user_condition');
            }
        }
        if(@$condition['phone'] != ''){
            $users = $users->where('phone','like',"%".$condition['phone']."%");
        }

        if(@$condition['type_login'] != ''){
            $users = $users->where('type_login','=',$condition['type_login']);
        }

        if(@$condition['email'] != ''){
            $users = $users->where('email','like',"%".$condition['email']."%");
        }

        $users = $users->paginate(20);
        // echo "<pre>";
        // print_r($users->link());
        // echo "</pre>";
        // die;
        if(Session::get('message')){
            $message = Session::get('message');
            Session::put('message', "");
        }else{
            $message = "";
        }

        return view('admin.pages.managerUsers',compact('menu','users','page' , 'condition' , 'message'));
    }


    public function login(Request $request)
    {
        if ($request->isMethod('post')) {
            $validator = \Validator::make($request->all(), [

                'email'    => 'required',
                'password' => 'required'
            ], [
                'email.required'    => 'Email bắt buộc',
                'password.required' => 'Password bắt buộc nhập'
            ]);
            if($validator->fails()) {
                $errors = $validator->errors();
                // print_r($errors);die;
                return view('admin.auth.login', compact('errors'));
            }

            if(Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                $user = User::where('email',$request->email)->first();
                $user->device_token = $request->device_token;
                $user->save();
                Auth::login($user);
                $user = Auth::user();
                $token = $user->createToken('LOGIN')->accessToken;
                $user['token'] = $token;

                return redirect('/admin/');
            }
            else
            {
                $message = "Tên đăng nhập hoặc mật khẩu không đúng";
                return view('admin.auth.login', compact('message'));
            }

        }
        $title = "";
        $users = "";

        return view('admin.auth.login', compact('title', 'users'));
    }





}
