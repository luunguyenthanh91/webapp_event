<?php

namespace App\Http\Controllers\Admin;

use App\Event;
use App\Gift;
use App\Country;
use App\Favorite;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Session;
use Excel;
class GiftController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // protected $redirectTo = '/dashboard';


    public function editAjax(Request $request,$id)
    {

           $event = Gift::find($id);

           if($event->status != 1){
              $event->status = 1;
           }else{
              $event->status = 2;
           }

           $event->save();



          return $this->dataSuccess('Thay Đổi Thông Tin Thành Công',[],200);
    }
    public function edit(Request $request,$id)
    {
          $menu = 'gifts';
          $message = '';
          $events = Event::orderby('id','DESC');
          $events = $events->where('status','=',1);
          $events = $events->get();

          if ($request->isMethod('post')) {
              $data = $request->data;
              $icon = '';


              if($request->hasFile('file_icon')){
                  $name = time();
                  $name .= '.png';
                  $path = $request->file('file_icon');
                  $destinationPath = 'uploads/images/';
                  $path->move($destinationPath,$name);
                  $icon = $destinationPath.$name;
               }

               $event = Gift::find($id);
               $event->name = $data['name'];
               if($icon != ''){
                  $event->icon = $icon;
               }
               $event->status = $data['status'];
               $event->event_id = $data['event_id'];
               $event->name_gift = $data['name_gift'];
               $event->save();

              Session::put('message', "Thay đổi quà tặng thành công.");
              return redirect('/admin/gift');
          }else{
            $data = Gift::find($id);
          }


          return view('admin.pages.gift.edit', compact('data', 'events' ,'menu','message'));
    }


    public function add(Request $request)
    {
        $menu = 'gifts';
        $message = '';
        $events = Event::orderby('id','DESC');
        $events = $events->where('status','=',1);
        $events = $events->get();

        if ($request->isMethod('post')) {
            $data = $request->data;
            $icon = '';
            $event = Event::find($data['event_id']);

            $gifts_ = Gift::where('event_id','=',$event->id)->get();
            $list_not_in = [];
            if($gifts_){
                foreach ($gifts_ as $key => $value) {
                    $value->list_number = explode(',',$value->list_number);
                    $list_not_in = array_merge($list_not_in,$value->list_number);
                }
            }

            $arr = [];
            for ($i = 1; $i <= $event->percent; $i++)
            {
                if(!in_array($i,$list_not_in)){
                    $arr[] = $i;
                }

            }
            // print_r($arr);die;



            $rand_key_s = array_rand($arr, $data['count']);
            // print_r($rand_key_s);die;
            // print_r($rand_key_s);die;
            if($data['count'] == 1){
                $rand_keys = $arr[$rand_key_s];
            }else if($data['count'] < 1){
                $rand_keys = 0;
            }else{
                $rand_keys = ',';
                foreach ($rand_key_s as $key => $value) {
                  $rand_keys .= $arr[$value].',';
                }
            }

            // print_r($rand_keys);die;
            if($request->hasFile('file_icon')){
                $name = time();
                $name .= '.png';
                $path = $request->file('file_icon');
                $destinationPath = 'uploads/images/';
                $path->move($destinationPath,$name);
                $icon = $destinationPath.$name;
             }

             $event = new Gift();
             $event->name = $data['name'];
             if($icon != ''){
                $event->icon = $icon;
             }
             $event->status = $data['status'];
             $event->count = $data['count'];
             $event->list_number = ','.$rand_keys.',';
             $event->event_id = $data['event_id'];
             $event->name_gift = $data['name_gift'];
             $event->save();

            Session::put('message', "Thêm quà tặng mới thành công.");

            return redirect('/admin/gift');
        }else{
          $data = [];
        }


        return view('admin.pages.gift.add', compact('data', 'events' ,'menu','message'));
    }


    public function delete(Request $request,$id){
        $user = Gift::where('id',$id)->first();

        $user->delete();
        Session::put('message', "Xoá quà tặng thành công.");



        return redirect('/admin/gift');

    }

    public function index(Request $request){
        $menu = 'gifts';
        $events = Event::orderby('id','DESC');
        $events = $events->where('status','=',1);
        $events = $events->get();

        if(!$request->page){
            $page = 1;
        }else{
            $page = $request->page;
        }
        $users = Gift::orderby('gift.id','DESC');
        // $users = $users->join('event','gift.event_id','=','event.id');
        if ($request->isMethod('post')) {
            $condition = @$request->condition;
            Session::put('gift_condition', $condition);
        }
        else{
            if(Session::get('gift_condition')){
                $condition = Session::get('gift_condition');
            }
        }

        if(@$condition['event_id'] != ''){
            $users = $users->where('gift.event_id','=',$condition['event_id']);
        }



        $users = $users->paginate(20);

        foreach ($users as $key => $value) {
          $users[$key]['event'] = Event::find($value['event_id']);
        }

        // echo "<pre>";
        // print_r($users);
        // echo "</pre>";
        // die;
        if(Session::get('message')){
            $message = Session::get('message');
            Session::put('message', "");
        }else{
            $message = "";
        }

        return view('admin.pages.gift.manager',compact('menu', 'events' ,'users','page' , 'condition' , 'message'));
    }





}
