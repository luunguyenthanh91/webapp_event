<?php

namespace App\Http\Controllers\Admin;

use App\Event;
use App\Country;
use App\Favorite;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Session;
use Excel;
class EventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // protected $redirectTo = '/dashboard';



    public function editAjax(Request $request,$id)
    {

           $event = Event::find($id);

           if($event->status != 1){
              $event->status = 1;
           }else{
              $event->status = 2;
           }

           $event->save();



          return $this->dataSuccess('Thay Đổi Thông Tin Thành Công',[],200);
    }

    public function edit(Request $request,$id)
    {
          $menu = 'events';
          $message = '';
          if(Session::get('message')){
              $message = Session::get('message');
              Session::put('message', "");
          }else{
              $message = "";
          }

          if ($request->isMethod('post')) {
              $data = $request->data;
              if($data['code'] && $data['code'] != ''){
                 $check_event_code = Event::where('code','=',$data['code'])->where('id','!=',$id)->first();
                 if($check_event_code){
                     $message = "Mã code này đã tồn tại.";
                 }else{
                   $data = $request->data;
                   $icon = '';
                   if($request->hasFile('file_icon')){
                       $name = time();
                       $name .= '.png';
                       $path = $request->file('file_icon');
                       $destinationPath = 'uploads/images/';
                       $path->move($destinationPath,$name);
                       $icon = $destinationPath.$name;
                    }

                    $event = Event::find($id);
                    $event->name = $data['name'];
                    if($icon != ''){
                       $event->icon = $icon;
                    }

                    $event->start_date = $data['start_date'];
                    $event->expired_at = $data['expired_at'];
                    $event->status = $data['status'];
                    $event->percent = $data['percent'];
                    $event->code = $data['code'];
                    $event->type = $data['type'];
                    $event->save();
                   Session::put('message', "Thay đổi sự kiện thành công.");
                   return redirect('/admin/events');
                 }

              }
              else{
                $data = $request->data;
                $icon = '';
                if($request->hasFile('file_icon')){
                    $name = time();
                    $name .= '.png';
                    $path = $request->file('file_icon');
                    $destinationPath = 'uploads/images/';
                    $path->move($destinationPath,$name);
                    $icon = $destinationPath.$name;
                 }

                 $event = Event::find($id);
                 $event->name = $data['name'];
                 if($icon != ''){
                    $event->icon = $icon;
                 }

                 $event->start_date = $data['start_date'];
                 $event->expired_at = $data['expired_at'];
                 $event->status = $data['status'];
                 $event->percent = $data['percent'];
                 $event->type = $data['type'];
                 $event->save();
                Session::put('message', "Thay đổi sự kiện thành công.");
                return redirect('/admin/events');
              }



          }else{
            $data = Event::find($id);
          }


          return view('admin.pages.edit_events', compact('data','menu','message'));
    }

    public function incrementalHash($len = 5){
      $charset = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
      $base = strlen($charset);
      $result = '';

      $now = explode(' ', microtime())[1];
      while ($now >= $base){
        $i = $now % $base;
        $result = $charset[$i] . $result;
        $now /= $base;
      }
      return substr($result, -5);
    }
    public function add(Request $request)
    {
        $menu = 'events';
        $message = '';
        if ($request->isMethod('post')) {
            $data = $request->data;
            $icon = '';


            if($request->hasFile('file_icon')){
                $name = time();
                $name .= '.png';
                $path = $request->file('file_icon');
                $destinationPath = 'uploads/images/';
                $path->move($destinationPath,$name);
                $icon = $destinationPath.$name;
             }

             $event = new Event();
             $event->name = $data['name'];
             if($icon != ''){
                $event->icon = $icon;
             }

             $event->start_date = $data['start_date'];
             $event->expired_at = $data['expired_at'];
             $event->status = $data['status'];
             $event->percent = $data['percent'];
             $event->type = $data['type'];
             $event->save();
             if($data['code'] && $data['code'] != ''){
                $check_event_code = Event::where('code','=',$data['code'])->where('id','!=',$event->id)->first();
                if($check_event_code){
                    Session::put('message', "Mã code này đã tồn tại.");
                    return redirect('/admin/edit-events/'.$event->id);
                }else{
                  $event->code = $data['code'];
                  $event->save();
                   Session::put('message', "Thêm sự kiện mới thành công.");
                   return redirect('/admin/events');
                }

             }
             else{
                 $event->code = $this->incrementalHash().$event->id;
                 $event->save();
                Session::put('message', "Thêm sự kiện mới thành công.");
                return redirect('/admin/events');
             }

        }else{
          $data = [];
        }


        return view('admin.pages.add_events', compact('data','menu','message'));
    }


    public function delete(Request $request,$id){
        $user = Event::where('id',$id)->first();

        $user->delete();
        Session::put('message', "Xoá sự kiện thành công.");



        return redirect('/admin/events');

    }

    public function index(Request $request){
        $menu = 'events';
        if(!$request->page){
          $page = 1;
        }else{
            $page = $request->page;
        }
        $users = Event::orderby('id','DESC');
        if ($request->isMethod('post')) {
            $condition = @$request->condition;
            Session::put('event_condition', $condition);
        }
        else{
            if(Session::get('event_condition')){
                $condition = Session::get('event_condition');
            }
        }

        if(@$condition['status'] != ''){
            $users = $users->where('status','=',$condition['status']);
        }
        if(@$condition['name'] != ''){
            $users = $users->where('name','like','%'.$condition['name'].'%');
        }



        $users = $users->paginate(20);
        // echo "<pre>";
        // print_r($users->link());
        // echo "</pre>";
        // die;
        if(Session::get('message')){
            $message = Session::get('message');
            Session::put('message', "");
        }else{
            $message = "";
        }

        return view('admin.pages.managerEvents',compact('menu','users','page' , 'condition' , 'message'));
    }





}
