<?php

namespace App\Http\Controllers\Admin;

use App\Ad;
use App\Contact_query;
use App\Payment;
use App\Report_ad;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index(){
        $menu = 'home';
        return view('admin.pages.dashboard',compact('menu'));
    }


    public function logout(){
        if (Auth::check()){
            Auth::logout();
        }
        return redirect('/admin/login');
    }
}
