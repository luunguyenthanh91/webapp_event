<?php

namespace App\Http\Controllers\API\V1;

use App\Ad;
use App\Event;
use App\Gift;
use App\History;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EventController extends Controller
{
    public function check(Request $request)
    {
        try{
            $event = Event::where('code','=' ,$request->code)->where('status','=' ,1)->first();
            if($event){
                return $this->dataSuccess('Lấy danh sách event thành công',$event,200);
            }else{
                return $this->dataError("Sự kiện không tồn tại hoặc đã kết thúc",null,200);
            }




        }catch (\Exception $exception)
        {
            return $this->dataError("Sự kiện không tồn tại hoặc đã kết thúc",null,200);
        }


    }
    public function historyGift(Request $request)
    {
        try{
            $history = History::where('user_id','=' ,$request->user_id)->orderby('id','DESC')->get();
            $data_return = [];
            foreach ($history as $key => $value) {
                $data_return[$key]['history'] = $value;
                $data_return[$key]['gifts'] = Gift::where('id','=' ,$value->gift_id)->first();
                $data_return[$key]['event'] = Event::where('id','=' ,$value->event_id)->first();
            }

            return $this->dataSuccess('Lấy Thông Tin Thành Công',$data_return,200);
        }catch (\Exception $exception)
        {
            return $this->dataError($exception->getMessage(),null,200);
        }


    }
    public function updateGift(Request $request)
    {
        try{
            $history = History::where('gift_id','=' ,$request->gift_id)->where('user_id','=' ,$request->user_id)->first();
            if($history){
              $history->name = $request->name;
              $history->email = $request->email;
              $history->phone = $request->phone;
              $history->address = $request->address;
              $history->save();
            }
            return $this->dataSuccess('Thay Đổi Thông Tin Thành Công',[],200);
        }catch (\Exception $exception)
        {
            return $this->dataError($exception->getMessage(),null,200);
        }


    }
    public function runAction(Request $request)
    {
        try{
            $event = Event::where('id','=' ,$request->event_id)->where('status','=' ,1)->first();

            if($event){
                if($event->type == 1){
                    $gifts = Gift::where('event_id','=' ,$request->event_id)->where('status','=' ,1)->first();
                    if($gifts){
                      $history = History::where('gift_id','=' ,$gifts->id)->where('user_id','=' ,$request->user_id)->first();
                      if($history){
                          return $this->dataError("Bạn đã hết lượt quay.",null,200);
                      }else{
                          $history = new History();
                          $history->user_id = $request->user_id;
                          $history->event_id = $event->id;
                          $history->gift_id = $gifts->id;
                          $history->save();
                          $count = History::where('gift_id','=' ,$gifts->id)->count();
                          $list_key = explode(',',$gifts->list_number);
                          if(in_array($count, $list_key) && $gifts->count > 0){
                              $gifts->count = $gifts->count-1;
                              $gifts->save();
                              $data = [];
                              $data['status'] = 2;
                              $history->status = 2;
                              $history->save();
                              return $this->dataSuccess('Lắc Thành Công',$data,200);
                          }else{
                            $data = [];
                            $data['status'] = 1;
                            $history->status = 1;
                            $history->save();
                            return $this->dataSuccess('Lắc Thành Công',$data,200);
                          }
                      }

                    }else{
                        return $this->dataError("Sự kiện đã kết thúc",null,200);
                    }
                }else{
                  $history = History::where('event_id','=' ,$request->event_id)->where('user_id','=' ,$request->user_id)->first();
                  if($history){
                      return $this->dataError("Bạn đã hết lượt quay.",null,200);
                  }else{
                      $history = new History();
                      $history->user_id = $request->user_id;
                      $history->event_id = $request->event_id;
                      $history->gift_id = 0;
                      $history->save();
                      $count = History::where('event_id','=' ,$request->event_id)->count();

                      $gifts_check = Gift::where('event_id','=' ,$request->event_id)->where('list_number','like' ,'%,'.$count.',%')->first();
                      // $list_key = explode(',',$gifts->list_number);
                      if($gifts_check && $gifts_check->count > 0){
                          $gifts_check->count = $gifts_check->count-1;
                          $history->gift_id = $gifts_check->id;
                          $gifts_check->save();
                          $data = [];
                          $data['gifts'] = $gifts_check;
                          $data['status'] = 2;
                          $history->status = 2;
                          $history->save();
                          return $this->dataSuccess('Lắc Thành Công',$data,200);
                      }else{
                        $data = [];
                        $data['status'] = 1;
                        $history->status = 1;
                        $history->save();
                        return $this->dataSuccess('Lắc Thành Công',$data,200);
                      }
                  }
                }



            }else{
                return $this->dataError("Sự kiện không tồn tại hoặc đã kết thúc",null,200);
            }




        }catch (\Exception $exception)
        {
            return $this->dataError($exception->getMessage(),null,200);
        }


    }

    public function checkGift(Request $request)
    {
        try{
            $event = Event::where('id','=' ,$request->event_id)->where('status','=' ,1)->first();

            if($event){
                if($event->type == 1){
                    $gifts = Gift::where('event_id','=' ,$request->event_id)->where('status','=' ,1)->first();
                    if($gifts){
                      $history = History::where('gift_id','=' ,$gifts->id)->where('user_id','=' ,$request->user_id)->first();
                      if($history){
                          return $this->dataError("Bạn đã hết lượt quay.",null,200);
                      }else{
                        $data = [];
                        $data['event'] = $event;
                        $data['gifts'] = $gifts;

                        return $this->dataSuccess('Lấy danh sách event thành công',$data,200);
                      }

                    }else{
                        return $this->dataError("Bạn đã hết lượt quay.",null,200);
                    }
                }else{
                    $history = History::where('event_id','=' ,$request->event_id)->where('user_id','=' ,$request->user_id)->first();

                    if($history){
                        return $this->dataError("Bạn đã hết lượt quay.",null,200);
                    }else{
                      $data = [];
                      $data['event'] = $event;
                      $data['gifts'] = ["icon"=> "uploads/images/gift_img.png","name" => "Giải Thưởng"];

                      return $this->dataSuccess('Lấy danh sách event thành công',$data,200);
                    }
                }


            }else{
                return $this->dataError("Sự kiện không tồn tại hoặc đã kết thúc",null,200);
            }




        }catch (\Exception $exception)
        {
            return $this->dataError("Sự kiện không tồn tại hoặc đã kết thúc",null,200);
        }


    }
}
