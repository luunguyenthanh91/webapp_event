<?php

namespace App\Http\Controllers\API\V1\Auth;

use App\User;
use App\Event;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Validator;
use Illuminate\Support\Facades\Hash;
class AuthController extends Controller
{
    public function register(Request $request)
    {
        try
        {
            $user_request = User::where('email',$request->email)->first();

            if($user_request)
            {
                return $this->dataError('Email đã tồn tại',null,200);
            }

            if($request->password != $request->re_password)
            {
                return $this->dataError('Mật khẩu không trùng khớp',null,200);
            }

            $user = new User();
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->save();


            return $this->dataSuccess('Đăng kí thành công',$user,200);
        }catch (\Exception $exception){
            return $this->dataError($exception->getMessage(),null,200);
        }
    }

    public function login(Request $request)
    {
        try
        {
          $user = User::where('email',$request->email)->first();
          if($user){
              if(Hash::check($request->password , $user->password)){
                  return $this->dataSuccess('Đăng nhập thành công',$user,200);
              }
              else
              {
                  return $this->dataError('Tên đăng nhập hoặc mật khẩu không đúng',null,200);
              }
          }else{
              return $this->dataError('Tên đăng nhập hoặc mật khẩu không đúng',null,200);
          }

        }catch (\Exception $exception)
        {
            return $this->dataError('Tên đăng nhập hoặc mật khẩu không đúng',null,200);
        }
    }
    public function msnv(Request $request)
    {
        try
        {
          $user = User::where('user_name',$request->msnv)->where('type_login','=' , 3)->first();
          if($user){
              $event = Event::where('id','=' ,$user->event_id)->first();
              $user->code_event = $event->code;
              return $this->dataSuccess('Đăng nhập thành công',$user,200);
          }else{
              return $this->dataError('Tài khoản không tồn tại',null,200);
          }

        }catch (\Exception $exception)
        {
            return $this->dataError('Tài khoản không tồn tại',null,200);
        }
    }





    public function facebook(Request $request)
    {
        try{
            $user_request = User::where('email',$request->email)->where('type_login','=' ,2)->first();

            if($user_request){
                return $this->dataSuccess('Đăng nhập thành công',$user_request,200);
            }else{
                $user = new User();
                $user->email = $request->email;
                $user->name = $request->name;
                $user->type_login = $request->type_login;

                $user->save();
                return $this->dataSuccess('Đăng nhập thành công',$user,200);
            }



        }catch (\Exception $exception){
            return $this->dataError($exception->getMessage(),null,200);
        }
    }


}
