<?php

namespace App\Http\Controllers\API\V1;

use App\Ad;
use App\Bid;
use App\Http\Controllers\Controller;
use App\Media;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;


class AuctionController extends Controller
{


    public function create(Request $request)
    {
        try{
            $auctions = null;

            $title = $request->ad_title;
            $slug = unique_slug($title);
            $data = [
                'title'             => $request->title,
                'slug'              => $slug,
                'description'       => $request->ad_description,
                'category_id'       => $request->category_id,
                'sub_category_id'   => $request->category,
                'brand_id'          => 0,
                'type'              => $request->type,
                'ad_condition'      => $request->condition,
                'price'             => $request->price,
                'is_negotiable'     => $request->is_negotialble,

                'seller_name'       => $request->seller_name,
                'seller_email'      => $request->seller_email,
                'seller_phone'      => $request->seller_phone,
                'country_id'        => $request->country,
                'state_id'          => $request->state,
                'city_id'           => $request->city,
                'address'           => $request->address,
                'video_url'         => $request->video_url,
                'category_type'     => 'classifieds',
                'price_plan'        => $request->price_plan,
                'mark_ad_urgent'    => $request->mark_ad_urgent,
                'status'            => '0',
                'user_id'           => Auth::user()->id,
                'latitude'          => $request->latitude,
                'longitude'         => $request->longitude,
                'expired_at'         => $request->date,
            ];


            $auctions = Ad::create($data);

            if (get_option('ads_moderation') == 'direct_publish'){
                $data['status'] = '1';
            }

            if ( ! $request->price_plan){
                $data['price_plan'] = 'regular';
            }


            $this->uploadAdsImage($request,$auctions->id);



            return $this->dataSuccess('Tạo đấu giá thành công',$auctions,200);
        }
        catch (\Exception $exception)
        {
            return $this->dataError($exception->getMessage(),null,200);
        }
    }

    public function search(Request $request){
        try{

            $auctions = Ad::orderby('id','DESC');


            if($request->category_id)
            {
              $auctions =  $auctions->where('category_id',$request->category_id);
            }
            if ($request->name)
            {
                $auctions = $auctions->where('title','like',"%$request->name%");
            }
            $dateNow = date("Y-m-d");
            $auctions = $auctions->where('expired_at','>=',$dateNow);
            $auctions = $auctions->paginate(10);

            return $this->dataSuccess('Lấy danh sách đấu giá thành công',$auctions,200);
        }
        catch (\Exception $exception)
        {
            return $this->dataError($exception->getMessage(),[],200);
        }
    }

    public function getDetail(Request $request){
        try{

            $auction = Ad::where('id',$request->id)->first();
            $auction->bids;

            return $this->dataSuccess('Lấy đấu giá chi tiết thành công',$auction,200);

        }
        catch (\Exception $exception)
        {
            return $this->dataError($exception->getMessage(),null,200);
        }
    }

    public function bid(Request $request){
        try{

           $bid = new Bid();
           $bid->user_id = Auth::user()->id;
           $bid->ad_id = $request->ad_id;
           $bid->bid_amount = $request->bid_amount;
           $bid->is_accepted = 1;
           $bid->save();
           $ads = Ad::find($request->ad_id);
           $ads->expired_at = date("Y-m-d", strtotime("-1 days"));
           $ads->save();
            $serviceAccount = ServiceAccount::fromJsonFile('../public/firebase/firebase.json');
            $firebase       = (new Factory())
                ->withServiceAccount($serviceAccount)
            ->createDatabase();
//            $db             = $firebase->getDatabase();
            $firebase->getReference('auctions/'.$request->ad_id.'/'.$bid->id)->set($bid);

           return $this->dataSuccess('Đấu giá chi tiết thành công',$bid,200);

        }
        catch (\Exception $exception)
        {
            return $this->dataError($exception->getMessage(),null,200);
        }
    }


    public function uploadAdsImage(Request $request, $ad_id = 0){
        $user_id = 0;

        if (Auth::check()){
            $user_id = Auth::user()->id;
        }

        if ($request->hasFile('images')){
            $images = $request->file('images');
            foreach ($images as $image){
                $valid_extensions = ['jpg','jpeg','png'];
                if ( ! in_array(strtolower($image->getClientOriginalExtension()), $valid_extensions) ){
                    return redirect()->back()->withInput($request->input())->with('error', 'Only .jpg, .jpeg and .png is allowed extension') ;
                }

                $file_base_name = str_replace('.'.$image->getClientOriginalExtension(), '', $image->getClientOriginalName());
                $resized = Image::make($image)->resize(640, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->stream();
                $resized_thumb = Image::make($image)->resize(320, 213)->stream();

                $image_name = strtolower(time().str_random(5).'-'.str_slug($file_base_name)).'.' . $image->getClientOriginalExtension();

                $imageFileName = 'uploads/images/'.$image_name;
                $imageThumbName = 'uploads/images/thumbs/'.$image_name;

                try{
                    //Upload original image
                    $is_uploaded = current_disk()->put($imageFileName, $resized->__toString(), 'public');

                    if ($is_uploaded) {
                        //Save image name into db
                        $created_img_db = Media::create(['user_id' => $user_id, 'ad_id' => $ad_id, 'media_name'=>$image_name, 'type'=>'image', 'storage' => get_option('default_storage'), 'ref'=>'ad']);

                        //upload thumb image
                        current_disk()->put($imageThumbName, $resized_thumb->__toString(), 'public');
                        $img_url = media_url($created_img_db, false);
                    }
                } catch (\Exception $e){
                    return redirect()->back()->withInput($request->input())->with('error', $e->getMessage()) ;
                }
            }
        }
    }

}
