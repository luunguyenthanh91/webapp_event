<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $guarded = [];

    public function getMediaNameAttribute($val)
    {
      return  $avatar = asset('/uploads/images/'.$val);
    }
}
