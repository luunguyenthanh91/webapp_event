<?php
namespace App\Helpers;


class ExportFile implements \FromArray, \WithHeadings, \WithBatchInserts, \WithChunkReading
{
    private $myArray;
    private $myHeadings;

    public function __construct($myArray, $myHeadings){
        $this->myArray = $myArray;
        $this->myHeadings = $myHeadings;
    }

    public function array(): array{
        return $this->myArray;
    }

    public function headings(): array{
        return $this->myHeadings;
    }
}
?>
