<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bid extends Model
{
    protected $guarded = [];
    protected $appends = ['user'];

    public function posting_datetime(){
        $created_date_time = $this->created_at->timezone(get_option('default_timezone'))->format(get_option('date_format_custom').' '.get_option('time_format_custom'));
        return $created_date_time;
    }

    public function getUserAttribute()
    {
      return  $user = User::select(['name','photo','id'])->where('id',$this->user_id)->first();
    }
}
